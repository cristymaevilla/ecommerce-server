const mongoose = require("mongoose");

const userSchema= new mongoose.Schema({
	firstName: {
	type:String,
	required : [true, "First name is required"]
	},
	lastName: {
	type:String,
	required : [true, "Last name is required"]
	},
	email: {
		type:String,
		required : [true, "Email is required"]
	},
	password: {
		type:String,
		required : [true, "Password is required"]
	},
	isAdmin:{
		type:Boolean,
		default:false,
	},
	isSubscriber:{
		type:Boolean,
		default:false,
	},
	mobileNo: {
		type:String,
		required : [true, "Mobile Number is required"]
	},
	completeAddress: {
		type:String,
		required : [true, "Address is required"]
	},
	orders: 
		[{
			orderId:{
			type:String,
			required:[true, " Id is required"]
				},
			createdOn: {
					type: Date,
					default: new Date()
				},
			status: {
					type: String,
					default: "completed"
				}
			}]
	
});


module.exports = mongoose.model("User", userSchema);