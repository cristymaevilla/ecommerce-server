const mongoose = require("mongoose");

const reviewSchema= new mongoose.Schema({
	userId: {
	type:String,
	required : [true, "First name is required"]
	},
	name: {
		type:String,
		default: "Majestea User"
	},
	rating: {
		type:Number,
		required : [true, "Rating is required"]
	},
	comment:{
		type:String,
		default: "no comment"
	}
	
});


module.exports = mongoose.model("Review", reviewSchema);