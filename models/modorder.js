const mongoose = require("mongoose");

const orderSchema= new mongoose.Schema({

	userId:{
		type:String,
		required: [true, "User Id method is required"]
	},

	items:[{	
		name:{type:String,
		default:""},
		quantity:{type:String,
		default:""},
		_id:{type:String,
		default:""}

	}
	
	],
	subTotal: {
		type:Number,
		required : [true, "Total amount is required"]
	},
	shippingFee: {
		type:Number,
		required : [true, "Total amount is required"]
	},
	totalAmount: {
		type:Number,
		required : [true, "Total amount is required"]
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},

	paymentMethod:{
		type:String,
		required: [true, "Payment method is required"]
	},
	isCancelled:{
		type:Boolean,
		default: false
	},

	status:{
		type:String,
		default: "to ship"
	}

});


module.exports = mongoose.model("Order", orderSchema);
