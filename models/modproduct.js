const mongoose = require("mongoose");

const productSchema= new mongoose.Schema({
	name: {
		type:String,
		required : [true, "Product name is required"]
	},

	category: {
		type:String,
		required : [true, "Category is required"]
	},
	description: {
		type:String,
		required : [true, "Description  is required"]
	},
	price: {
		type:Number,
		required : [true, "Price is required"]
	},
	isActive: {
			type:Boolean,
			default:true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	stocks:{
		type:Number,
		required : [true, "Number of stock is required"]
	},
	src:{
		type:String,
		required : [true, "Image source is required"]
	},
	reviews:[{
		userId:{
			type:String,
			required : [true, "User Id name is required"]
		},
		userName:{
			type:String,
			default :"Majestea User"
		},
		name:{
			type:String,
			default:"Majestea user"
		},
		rating:{
			type:Number,
			required : [true, "Rating is required"]
			
		},
		comment: {
			type:String,
			default:"No comment"
		}
	}]


});


module.exports = mongoose.model("Product", productSchema);