const express = require("express");
const router = express.Router();

const productController= require("../controllers/product");
const auth= require("../auth");



// Create/Add product (ADMIN only):-------------------
router.post("/add", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	productController.addProduct(user, req.body).then(resultFromController => res.send(resultFromController));
});

// RETRIEVE ALL ACTIVE products:----------------------
router.get("/", (req,res)=>{
	productController.getAllActiveProducts().then(resultFromController => res.send(
		resultFromController));
})
router.get("/all", (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.getAll().then(resultFromController => res.send(
		resultFromController));
})
// RETRIEVE REVIEWS (admin)---------------------------
router.get("/reviews/all", (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.getReviews().then(resultFromController => res.send(
		resultFromController));
})
router.get("/reviews/by-product", (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.getReviewsByProduct().then(resultFromController => res.send(
		resultFromController));
})
router.get("/reviews/by-product-id", (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.getReviewsByProductId(req.body).then(resultFromController => res.send(
		resultFromController));
})
// FEATURE REVIEW
router.post("/reviews/feature", (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.featureReview(req.body).then(resultFromController => res.send(
		resultFromController));
})
// DELETE REVIEW FROM FEATURE REVIEW
router.delete("/reviews/feature/remove", (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.deleteFeature(user,req.body).then(resultFromController => res.send(
		resultFromController));
})
// RETRIEVE REVIEWS (user)---------------------------
router.get("/reviews/user", (req,res)=>{
	const user = auth.decode(req.headers.authorization)
	productController.getReviewsByUser(user).then(resultFromController => res.send(
		resultFromController));
})
// RETRIEVE by category:----------------------
// router.get("/category", (req,res)=>{
// 	productController.getByCategory(req.body).then(resultFromController => res.send(
// 		resultFromController));
// })

router.get("/category/fiction", (req,res)=>{
	productController.getAllFiction().then(resultFromController => res.send(resultFromController));})
router.get("/category/nonFiction", (req,res)=>{
	productController.getAllNonFiction().then(resultFromController => res.send(resultFromController));})
router.get("/category/greenTea", (req,res)=>{
	productController.getGreenT().then(resultFromController => res.send(resultFromController));})
router.get("/category/blackTea", (req,res)=>{
	productController.getBlackT().then(resultFromController => res.send(resultFromController));})
router.get("/category/fruitTea", (req,res)=>{
	productController.getFruitT().then(resultFromController => res.send(resultFromController));})
router.get("/category/herbalTea", (req,res)=>{
	productController.getHerbalT().then(resultFromController => res.send(resultFromController));})

// RETRIEVE SINGLE product:---------------------------
router.get("/:productId", (req,res)=>{
	productController.getProduct(req.params).then(resultFromController => res.send(
		resultFromController));
})

// UPDATE product information (ADMIN only):------------
router.put("/:productId/update", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	productController.updateProduct(user,req.params, req.body).then(resultFromController => res.send(
		resultFromController));
})

// ARCHIVE product
router.put("/:productId/archive", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	productController.archiveProduct(user,req.params).then(resultFromController => res.send(
		resultFromController));
})  
router.put("/:productId/activate", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	productController.activateProduct(user,req.params).then(resultFromController => res.send(
		resultFromController));
}) 
// ADD review by user
router.put("/:productId/addReview", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	productController.addReview(user,req.params, req.body).then(resultFromController => res.send(
		resultFromController));
})


// DELETE review by user
router.put("/:productId/:reviewId/delete", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	productController.deleteReview(user,req.params).then(resultFromController => res.send(
		resultFromController));
})




module.exports=router