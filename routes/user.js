const express = require("express");
const router = express.Router();

const userController= require("../controllers/user");
const auth= require("../auth");

// Route for checking if user's email already exists in the database
router.post("/checkEmail", (req,res)=> {
	userController.checkEmailExists(req.body).then(resultFromController=> res.send(resultFromController));
});

// REGISTER a user:-----------------
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController));
})

// LOG IN: authenticating a user:-------------
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController=> res.send(resultFromController));
})
// RETRIEVE ALL USERS (non ADmin)-----------------
// router.get("/all", (req,res)=>{
// 	userController.allUsers().then(resultFromController=> res.send(resultFromController));
// })
router.get("/all", (req,res)=>{
	userController.getAllUsers().then(resultFromController=> res.send(resultFromController));
})
router.get("/all/subscribers", (req,res)=>{
	userController.getAllSubscribers().then(resultFromController=> res.send(resultFromController));
})
// RETRIEVE user data:---------------
router.get("/details", auth.verify,(req,res)=>{
	const userData= auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id}).then(resultFromController=> res.send(resultFromController));
})
// // RETRIEVEall users by ADMIN:---------------
// router.get("/all", auth.verify,(req,res)=>{
// 	const userData= auth.decode(req.headers.authorization);
// 	userController.getProfile({userId : userData.id}).then(resultFromController=> res.send(resultFromController));
// })

router.put("/setAdmin", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	userController.setAdmin(user,req.body).then(resultFromController => res.send(
		resultFromController));
})





// REMOVE  USER as admin (ADMIN only):----------------
router.put("/:userId/removeAdmin", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	userController.removeAdmin(user,req.params).then(resultFromController => res.send(
		resultFromController));
})


// UPDATE USER profile (Users only)
router.put("/update", auth.verify,(req,res)=>{
	const userData= auth.decode(req.headers.authorization);
	userController.updateProfile(userData,req.body).then(resultFromController=> res.send(resultFromController));
})

// CHange password---------------------------

router.put("/changePassword", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)

	userController.changePassword(user,req.body).then(resultFromController => res.send(
		resultFromController));
})












module.exports=router