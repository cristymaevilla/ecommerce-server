const express = require("express");
const router = express.Router();

const cartController= require("../controllers/cart");
const auth= require("../auth")

// RETRIEVE ALL Items in user's cart/myCart (user only)--------------------
router.get("/", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	cartController.getAllItemsInMyCart(user).then(resultFromController => res.send(
		resultFromController));
})

router.get("/:itemId", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	cartController.item(user,req.params).then(resultFromController => res.send(resultFromController));
});


// UPDATE ITEM in CART:(update the quantity of the items in cart)
router.put("/:itemId/update", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	cartController.updateItem(user,req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// ADD product to User's cart/myCart(user only)------------------------------------------
router.put("/:productId/addtocart", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	cartController.addCart(user,req.params, req.body).then(resultFromController => res.send(
		resultFromController));
})


// DELETE ALL ITEMS IN USER's cart/myCart (user only)-------------------------------------
router.delete("/deleteAll", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	cartController.deleteAll(user).then(resultFromController => res.send(
		resultFromController));
})

// DELETE an item in user's cart/myCart (user only)
// NOTE: this wasn't used in the client side (no need to use this)

router.delete("/:itemId/delete", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	cartController.deleteItem(user,req.params).then(resultFromController => res.send(
		resultFromController));
})




module.exports=router