
const Product = require("../models/modproduct");
const Review = require("../models/modreviews");

const auth= require("../auth");


// CREATE a product (ADMIN ONLY)------------------------ok
module.exports.addProduct = async (user, reqBody) => {
	if(user.isAdmin){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			category: reqBody.category,
			price: reqBody.price,
			src: reqBody.src,
			stocks: reqBody.stocks
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return (true);
			}
		})
	}
	else{
		return (`You have no access`);
	}
}

// RETRIEVE ALL ACTIVE products---------------------------ok
module.exports.getAllActiveProducts = ()=> {
		return Product.find({isActive : true}).then(result => {
			return result;
		})
}
module.exports.getAll = ()=> {
		return Product.find().then(result => {
			return result;
		})
}
// RETRIEVE REVIEWS (user)-------------------------------------------
module.exports.getReviewsByUser = (user)=> {
		return Product.find().then(result => {
			let arr =[]
			// let arr2;
			result.map(product =>{ 
				let reviews = product.reviews;
				for (let i = 0; i < reviews.length; i++) {
					if(reviews[i].userId === user.id){
						arr.push({
								id: reviews[i]._id,
								product: product.name,
								rating: reviews[i].rating , 
								comment:reviews[i].comment,
								userId: reviews[i].userId
							})
					}
					
				 }
			 })
			return arr;
		})
}

// RETRIEVE REVIEWS (admin)-------------------------------------------
module.exports.getReviews = ()=> {
		return Product.find().then(result => {
			let arr =[]
			// let arr2;
			result.map(product =>{ 
				product.reviews.map(review =>{ 
					arr.push({
							id: review._id,
							rating: review.rating , 
							comment:review.comment,
							name:review.name,
							userId: review.userId
						})
				 })
			 })
			let count =arr.length;
			let ave= arr.reduce((acc,curr)=> ({rating: acc.rating + curr.rating}));
						ave= parseInt(ave.rating/arr.length);
			console.log({reviews: arr, average:ave, count:count});
			return{reviews: arr, average:ave, count:count}
		})
}
// RETRIEVE REVIEWS GROUP INTO PRODUCTS (admin)------------------------------------------
module.exports.getReviewsByProduct = ()=> {
		return Product.find().then(result => {
			let arr =[]
			// let arr2;
			result.map(product =>{
				if(product.reviews.length !== 0) {
					let ave= product.reviews.reduce((acc,curr)=> ({rating: acc.rating + curr.rating}));
								ave= parseInt(ave.rating/product.reviews.length);
					arr.push({
							name: product.name,
							reviews:product.reviews,
							averageRating:ave
						})
				}
		
			 })
			console.log(arr);
			return{arr}
		})
}
// RETRIEVE REVIEWS BY PRODUCT CATEGORY (admin)------------------------------------------
module.exports.getReviewsByProductId = (reqBody)=> {

		return Product.findById(reqBody.id).then(product => {		
			let count =product.reviews.length;	
				if(count !== 0) {
					let ave= product.reviews.reduce((acc,curr)=> ({rating: acc.rating + curr.rating}));
								ave= parseInt(ave.rating/product.reviews.length);
					return {ave:ave, reviews: product.reviews, count:count}
				}
				else{return false}
		
		})
}
//  FEATURE REVIEW(admin)-------------------------------------------
// module.exports.addReviewToReviewSchema = (user, reqBody)=> {
// 	if(user.isAdmin){
// 		return Product.find().then(result => {
// 			let review;
// 			// let arr2;
// 			result.map(product =>{
// 				let reviews=product.reviews
// 				for (let i = 0; i < reviews.length; i++) {
// 				    if (reviews[i]._id === reqBody.reviewId) {
// 				    	console.log(reviews[i])
// 				        // review= {name:reviews[i].name,
// 				        // 		rating: reviews[i].rating,
// 				        // 		comment: reviews[i].comment}
// 				    }
// 				}
		
// 			 })
// 			// console.log(review);
// 		})
// 	}
// }
// RETRIEVE REVIEWS (user)-------------------------------------------
module.exports.featureReview = (reqBody)=> {
		return Product.find().then(result => {
			
			result.map(product =>{ 
				let reviews = product.reviews;

				if (reviews.length !== 0){
					for (let i = 0; i < reviews.length; i++) {
						if(reviews[i].id === reqBody.id){
							let newReview = new Review({
								id: reviews[i]._id,
								rating: reviews[i].rating , 
								comment:reviews[i].comment,
								name:reviews[i].userName,
								userId: reviews[i].userId
								})
							return newReview.save().then((review, error) => {
								if(error){return false;}
								else{return true;}
							}) 
							}
						}
				}
				
			 })
		})
}
// DELETE FROM FEATURES:
module.exports.deleteFeature= (user, reqBody) => {
		let reviewId=reqBody.id;
		console.log(reqBody.id);
		 return Review.findByIdAndDelete(reviewId);
}

// RETRIEVE by category---------------------------ok
module.exports.getByCategory = (reqBody)=> {
	
		return Product.find(reqBody).then(result => {
			return result;
		})
}

module.exports.getAllFiction = ()=> {
	
		return Product.find({category:"fiction"}).then(result => {
			return result;
		})
}
module.exports.getAllNonFiction = ()=> {
	
		return Product.find({category:"non-fiction"}).then(result => {
			return result;
		})
}
module.exports.getGreenT = ()=> {
	
		return Product.find({category:"green tea"}).then(result => {
			return result;
		})
}
module.exports.getBlackT = ()=> {
	
		return Product.find({category:"black tea"}).then(result => {
			return result;
		})
}
module.exports.getFruitT = ()=> {
	
		return Product.find({category:"fruit tea"}).then(result => {
			return result;
		})
}
module.exports.getHerbalT = ()=> {
	
		return Product.find({category:"herbal tea"}).then(result => {
			return result;
		})
}
// RETRIEVE SINGLE product:------------------------------ok
module.exports.getProduct= async (reqParams)=>{
	
		let product=await Product.findById(reqParams.productId).then(result =>{
		return result;
		});

		console.log(product);
		let reviews = product.reviews
			console.log(reviews);

	if(reviews == ""){return {  
					Reviews:"no reviews",
					averageRating:0,
					product}}
	else { let ave= reviews.reduce((acc,curr)=> ({rating: acc.rating + curr.rating}));
			ave= parseInt(ave.rating/reviews.length);
			console.log(ave);
		return {  Reviews:reviews.length,averageRating:ave,product}
	}
}

// UPDATE product information (ADMIN only):--------------ok
module.exports.updateProduct= async (user, reqParams, reqBody)=>{
	if(user.isAdmin){
		// Specify the fields of the doc. to be updated
		let updatedProduct = {
			name: reqBody.name,
			category: reqBody.category,
			description:reqBody.description,
			price: reqBody.price,
			src: reqBody.src,
			stocks:reqBody.stocks
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error)=>{
			if(error){return false;}
			else{return (true);}
		})
	}
	else{
		return (`Sorry, you have no access`);
	}
}

// ARCHIVE product (ADMIN only)--------------------------ok
module.exports.archiveProduct= async (user, reqParams)=>{
	if(user.isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, {isActive :false} ).then((course,error)=>{
			
			if(error){return false;}
			else{return true;}
		})
	}
	else{
		return (`Sorry, you have no access`);
	}
}


module.exports.activateProduct= async (user, reqParams)=>{
	if(user.isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, {isActive :true} ).then((course,error)=>{
			
			if(error){return false;}
			else{return true;}
		})
	}
	else{
		return (`Sorry, you have no access`);
	}
}
// ADD REVIEWS by USER:----------------------------------ok
module.exports.addReview= async (user, reqParams, reqBody)=>{
	if(user.isAdmin){return (`Sorry, you have no access`) }	
	else{
		let review = {
			userId: user.id,
			name: reqBody.name,
			rating: reqBody.rating,
			comment: reqBody.comment,}
console.log(user.id);
		if(reqBody.rating < 0 || reqBody.rating > 5){return (false)}
		else{
			return Product.findById(reqParams.productId).then(product => {
				let reviews = product.reviews;
				console.log(product)
				reviews.push(review);
			return product.save().then((user, error) => {
				if(error){return false;}
				else{return true;}
			})
		})

		}
		
}
}

 // --------------------------------------------ok
module.exports.deleteReview= async (user, reqParams)=>{
	if(user.isAdmin){return (`Sorry, you have no access`) }	
	else{
		console.log(reqParams.reviewId);
		return Product.updateOne({_id:reqParams.productId}, {$pull: {reviews:{_id:reqParams.reviewId}}}).then((user, error) => {
				if(error){return false;}
				else{return ('Review deleted');}
			})

	}
}