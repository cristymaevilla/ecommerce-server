const Cart = require("../models/modcart");
const Order=require("../models/modorder");
const Product = require("../models/modproduct");
const User = require("../models/moduser");
const auth= require("../auth");


// CREATE ORDER-------------------------ok

module.exports.createOrder = async (user,reqParams,reqBody) => {

	if(user.isAdmin) {return (`You have no access`);}

	else{

		let cartItem=await Cart.findById(reqParams.itemId).then(cartItem=>{
			return cartItem;
		})

		let shippingFee=  (cartItem.subTotal > 500 )? 0 : 150;
		let totalAmount= shippingFee+cartItem.subTotal;
		let newOrder = new Order ({
			userId: user.id,
			productId:cartItem.productId,
			itemId:reqParams.itemId,
			quantity: cartItem.quantity,
			price: cartItem.price,
			subTotal: cartItem.subTotal,
			shippingFee:shippingFee,
			totalAmount:totalAmount,
			purchasedOn: new Date,
			paymentOption:reqBody.paymentOption,
			status: "pending"})
			
		console.log({newOrder});
		return newOrder.save().then((user,error)=>{
		if (error){return false;}

		else{ return { newOrder , message:"Checkout completed!"}}
	})
	}
}

module.exports.makeOrder = async (user, reqBody) => {

	if(user.isAdmin) {return (`You have no access`);}

	else{

		let newOrder = new Order ({
			userId: user.id,
			items:[{
			name:reqBody.name,
			quantity:reqBody.quantity,
			_id:reqBody._id}],
			subTotal: reqBody.subTotal,
			shippingFee:reqBody.shippingFee,
			totalAmount:reqBody.totalAmount,
			purchasedOn: new Date,
			paymentMethod:reqBody.paymentMethod,
			status: "to ship"})
			
		console.log({newOrder});
		return newOrder.save().then((user,error)=>{
		if (error){return false; }

		else{ return true; console.log(newOrder)}
	})
	}
}

// UPDATE ORDER TO USER: -------------------ok
/*module.exports.updateUserOrders = async (user, reqParams) => {


	if(user.isAdmin){
		console.log(reqParams.orderId);
		let order=await Order.findById(reqParams.orderId).then(result=>{
			return result;
		})
		User.findById(order.userId).then(user => {
			user.orders.push({orderId : reqParams.orderId});
			return user.save().then((user, error) => {
				if(error){return false;}
				else{return ('Got it!');}
			})
		})
	}
	else {return (`You have no access`);}
}*/

// UPDATE user, product and cart----------ok
module.exports.updateUserOrdersCart= async (user, reqParams)=>{
	


	if(user.isAdmin){
		let order=await Order.findById(reqParams.orderId).then(result=>{
			return result;
		})
	console.log(order);
		let isUserUpdated = await User.findById(order.userId).then(user => {	
			user.orders.push({orderId : reqParams.orderId});
			return user.save().then((user, error) => {
				if(error){return false;}
				else{return true;}
			})
		})

		let isCartUpdated = await Cart.findByIdAndRemove(order.itemId).then((result, error) => {
				if(error){return false;}
				else{return true;}
			})
		
		if(isUserUpdated && isCartUpdated ){return ("Everything is updated");}
		else{return false;}
		
	}
	else {return (`You have no access`);}
}
// PRODUCT UPDATE--------------------------------------
module.exports.updateProduct= async (user, reqParams)=>{
	


	if(user.isAdmin){

		let order=await Order.findById(reqParams.orderId).then(result =>{
		return result;})
		 let product=await Product.findById(order.productId).then(result =>{
		return result;})
			console.log(product);
			console.log(order);
		return Product.findByIdAndUpdate(order.productId, {stocks: product.stocks-order.quantity}).then((course,error)=>{
			if(error){return false;}
			else{ return ('Product updated')}
			})
	}	
	else {return (`You have no access`);}	
				
	}

// CANCELL pending orders:------------------------ ok
module.exports.cancel = async (user,reqParams) => {

	if(user.isAdmin) {return (`You have no access`);}

	else{ let data=await Order.findById(reqParams.orderId).then(result =>{
		return result;})
			let status= data.status;
			console.log(status);
		if (status==="pending"){return Order.findByIdAndUpdate(reqParams.orderId, {isCancelled: true}).then((course,error)=>{
			if(error){return false;}
			else{ return ('Order has been cancelled')}
			})}

		else{return ("Order cancellation denied.")}

}
}


// FIND Users orders by user--------------------ok
module.exports.all = async (user) => {
	if(user.isAdmin) {return (`You have no access`);}
	else{
		return Order.find({userId:user.id, isCancelled: false}).then(result =>{
		return result;
			})
	}

}
// RETRIEVE USERS TO SHIP ORDERS------------------------
module.exports.userToShip = async (user) => {
	if(user.isAdmin) {return (`You have no access`);}
	else{
			return Order.find({userId:user.id, status: "to ship"}).then(result =>{
			// console.log(result.length);
			if(result.length === 0 ){return false;}
			else{ return result;}
			})
	}

}

// RETRIEVE USERS DELIVERED ORDERS------------------------
module.exports.userDelivered = async (user) => {
	if(user.isAdmin) {return (`You have no access`);}
	else{
		return Order.find({userId:user.id, status: "delivered"}).then(result =>{
			console.log(result);
		if(result.length === 0 ){return false;}
		else{ return result;}
			})
	}

}


// RETRIEVE ALL ORDERS by admin---------------------------ok
module.exports.allOrders = async (user)=> {
	if(user.isAdmin){
		return Order.find().then(result => {
			return result;
		})
	}
	else{return (`You have no access`);}
}

// RETRIEVE ALL DELIVERED ORDERS by admin---------------------------ok
module.exports.allDeliveredOrders = async (user)=> {
	if(user.isAdmin){
		return Order.find({status: "delivered"}).then(result => {
		if(result.length === 0 ){return {message : "No delivered orders yet"}}
		else{ return result;}
		})
	}
	else{return false;}
}
// RETRIEVE ALL TO SHIP ORDERS by admin---------------------------ok
module.exports.allToShipOrders = async (user)=> {
	if(user.isAdmin){
		return Order.find({status: "to ship"}).then(result => {
		if(result.length === 0 ){return {message : "All orders were shipped"}}
		else{ return result;}
		})
	}
	else{return false;}
}

// RETRIEVE USER'S ORDERS by ADMIN-----------------------ok

module.exports.usersOrders = async (user, reqBody)=> {
	if(user.isAdmin){
		return Order.find({userId:reqBody.userId}).then(result => {
			return result;
		})
	}
	else{return (`You have no access`);}
}


// RETRIEVE ONE ORDER by user---------------------------ok
module.exports.retrieveOrder = async (user, reqParams)=> {
	if(user.isAdmin){(`You have no access`)}
		
		
	else{
		return Order.findById(reqParams.orderId).then(result =>{
		return result;
		})

	}
}
// UPDATE ORDER STATUS to completed by admin------ok

module.exports.updateComplete = async(user,reqParams, reqBody)=> {
	if(user.isAdmin){
		let data ={status: reqBody.status,}
		return Order.findByIdAndUpdate(reqParams.orderId, data).then((res,error)=>{
			if(error){return false;}

			else{ return (true);}
		})
	}
	else{return (`You have no access`);}
}