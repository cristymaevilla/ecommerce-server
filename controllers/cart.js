const Cart = require("../models/modcart");
const Order=require("../models/modorder");
const Product = require("../models/modproduct");
const User = require("../models/moduser");
const auth= require("../auth");


// RETRIEVE ALL ITEMS in User's myCart (USER)----------------------ok

module.exports.getAllItemsInMyCart = async (user)=> {
	let userCart=[];
		userCart= await Cart.find({userId:user.id}).then(result => {
			return result;})
	console.log(userCart);
	if(user.isAdmin) {return (`You have no access`);}
	if(userCart== "") {return false;}
	else{
		return  userCart ;}
				
}

// Add item to User's CART (USER)------------------------------------------ok
module.exports.addCart = async (user,reqParams, reqBody) => {

	if(user.isAdmin) {return (`You have no access`);}

	else{

		let product= await Product.findById(reqParams.productId).then(product=>{
			return product;
		})

		let subTotal= reqBody.quantity*product.price;

		let newCart = new Cart ({
			userId: user.id,
			productId:product.id,
			name: product.name,
			quantity: reqBody.quantity,
			stocks:product.stocks,
			price: product.price,
			subTotal: subTotal})
		console.log({newCart});
		return newCart.save().then((user,error)=>{
		if (error){return false;}

		else{ return {id: newCart.id, message:'Item was successfully added to cart.'}, console.log("addedtocart")}
	})
	}
}

// updates the subtotal and price when the quantity of item changes
// UPDATE ITEM IN CART-----------------------------ok
module.exports.updateItem = async (user,reqParams, reqBody) => {

	if(user.isAdmin) {return (`You have no access`);}

	else{

		let item= await Cart.findById(reqParams.itemId).then(data=>{
			return data;
		})

		let subTotal= reqBody.quantity*item.price;

		let updatedItem ={
			quantity: reqBody.quantity,
			price: item.price,
			subTotal: subTotal}
			return Cart.findByIdAndUpdate(reqParams.itemId, updatedItem).then((course,error)=>{
			if(error){return false;}

		else{ return console.log(subTotal, reqBody.quantity) }
	})
	}
}

	

// RETRIEVE AN ITEM------------------------ok
module.exports.item = async (user, reqParams) => {
	if(user.isAdmin) {return (`You have no access`);}
	else{
		return Cart.findById(reqParams.itemId).then(result =>{
		return result;
			})
	}

}

// note: needs to check which of these two was used in client-side
// ------------------------------------------------
module.exports.deleteAll= async (user) => {
	if(user.isAdmin) {return (`You have no access`);}
	else{
	
		Cart.findByIdAndRemove({userId:user.id}, (error,data)=>{
				if(error){return false;}
				else{return true;}
			})
	}

}
// ----------------------------------------------
module.exports.deleteAll= async (user) => {
	if(user.isAdmin) {return (`You have no access`);}
	else{
	
		Cart.deleteMany({userId:user.id}, (result, error)=>{
				if(error){return false;}
				 else{console.log("Successful deletion");}
			})
	}

}

// NOTE:this was not used in client-side
// ------------------------------------------------ok
module.exports.deleteItem = async (user, reqParams) => {
	if(user.isAdmin) {return (`You have no access`);}
	else{
		Cart.findByIdAndRemove(reqParams.itemId, (error,removedItem)=>{
				if(error){return false;}
				else{return removedItem;}
			})
	}
}


