const Cart = require("../models/modcart");
const Order=require("../models/modorder");
const Product = require("../models/modproduct");
const User = require("../models/moduser");

const bcrypt =require("bcrypt");
const auth= require("../auth");

// Check if the email already exists----------------ok
module.exports.checkEmailExists =(reqBody) =>{
	
	return User.find({email: reqBody.email}).then(result=>{
		// 
		if (result.length > 0){
			return true;}
		else{return false;}
	})
}

// User Registration------------------------------------ok
module.exports.registerUser=(reqBody)=>{
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		completeAddress: reqBody.completeAddress,
		// to encrypt the password; 10-no.of salt rounds
		password:bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error)=>{
		if (error){return false;}

		else{ return true;}
	})
}

// CHANGE PASSWORD (user) : ERROR (.) between reqBody & changePassword
/*module.exports.changePassword= async (userData, reqBody)=>{

	if({reqBody.currentPassword:""} || {reqBody.newPassword:""})
		{return "Please fill in all fields."}
	else{

		return User.findByIdAndUpdate(userData.id).then(result=>{

			bcypt.compareSync(reqBody.currentPassword, result.password), (err, isMatch)=>{

				if (err) {return err}
				if(isMatch){ let newpass = {password: bycrpt.hashSync(reqBody.newPassword,10)}
					return newpass.save().then((result,error)=>{
						if (error){return false;}

						else{ return `You have successfully updated your password`;}
						}
					}
				}
				else{return:"Current password is not a match"}
			}

		}

}*/
/*module.exports.allUsers = ()=> {
	
		return User.find({isAdmin: false}).then(result => {
			return result;
		})
}*/


// LOGIN: User authentication--------------------------------------------ok
module.exports.loginUser=(reqBody)=>{
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return "You are not yet registered";
		}
		else{
			const isPasswordCorrect= bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result),
					message: "Welcome back!"}

			}
			else{ return ("Please enter a valid password");}
		}
	})
}

// RETRIEVE user data: -----------------------------------------------ok
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};
// RETRIEVE all USERS: -----------------------------------------------ok
module.exports.getAllUsers = () => {
	return User.find({isAdmin:"false"}).then(result => {
		let count = result.length
		console.log(count);
		return {users:result, count:count};
	});
}
module.exports.getAllSubscribers = () => {
	return User.find({isSubscriber:"true"}).then(result => {
		let count = result.length
		console.log(count);
		return {users:result, count:count};
	});
}
// SET USER AS ADMIN (ADMIN only):--------------------------------------ok

module.exports.setAdmin= async (user, reqBody)=>{
	if(user.isAdmin){
		let userId= reqBody.userId;
		console.log(userId)
		let data=  await User.findById(userId).then(result=>{
			return result;})
		return User.findByIdAndUpdate(userId, {isAdmin :true} ).then((data,error)=>{
			
			if(error){return false;}
			else{return (`You have successfully added ${data.firstName} as admin.`);}
		})
	}
	else{
		return (`Sorry, you have no access`);
	}
}



// REMOVE as ADMIN
module.exports.removeAdmin= async (user, reqParams)=>{
	if(user.isAdmin){
		return User.findByIdAndUpdate(reqParams.userId, {isAdmin :false} ).then((data,error)=>{
			
			if(error){return false;}
			else{return (`You have successfully removed ${data.firstName} as admin.`);}
		})
	}
	else{
		return (`Sorry, you have no access`);
	}
}
// UPDATE USER PROFILE (user only)-------------------------------ok
module.exports.updateProfile= async (userData, reqBody)=>{
		console.log(userData);
		let updatedProfile = {
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			completeAddress: reqBody.completeAddress
		}
		return User.findByIdAndUpdate(userData.id, updatedProfile).then((course,error)=>{
			if(error){return false;}
			else{return 'You have successfully updated your profile';}
		})
	
}
